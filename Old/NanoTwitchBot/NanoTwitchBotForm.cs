﻿/*
 This is work of Josef "NanoMesh" Reichelt.
 Licensed under: https://creativecommons.org/licenses/by-nc-sa/4.0/
 More of my work can be found at https://www.artstation.com/artist/nanomesh and
 https://nanomesh.itch.io/.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;

namespace NanoTwitchBot
{
    public partial class NanoTwitchBotForm : Form
    {
        BotCore botCore;
        public TextBox chatDisplayBox;
        public TextBox debugDisplayBox;
        public string InputString { get; set; }



        private void NanoTwitchBotForm_Load(object sender, EventArgs e)
        {

        }

        //Inicializace
        public NanoTwitchBotForm()
        {
            InitializeComponent();

            botCore = new BotCore(chatBox,chatOnlineUsers,cookieTimer,fillerTimer,monsterLabel);
            botCore.BotInit();
        }
        //Bot loop
        public void botTimer_Tick(object sender, EventArgs e)
        {
            botCore.BotLoop();
        }

        private async void saveUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
           saveUsersToolStripMenuItem.Enabled = false;
           await botCore.RefreshUsers();
            saveUsersToolStripMenuItem.Enabled = true;
        }

        private async void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileToolStripMenuItem.Enabled = false;
            await botCore.RefreshUsers();
            await botCore.BotQuit();
            Application.Exit();
        }

        private void sendCookieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            botCore.SendCookieNow();
        }

        private void sendMonsterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            botCore.SendMonsterNow();
        }
    }
}
