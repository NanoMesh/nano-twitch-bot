﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NanoTwitchBot
{
    public partial class InputBox : Form
    {
        
        public NanoTwitchBotForm mainForm;

        public InputBox(NanoTwitchBotForm frm)
        {
            InitializeComponent();
            mainForm = frm;

            
        }


        private void butCancel_Click(object sender, EventArgs e)
        {
            mainForm.InputString = null;
            this.Hide();
        }

        public void butOk_Click(object sender, EventArgs e)
        {
            mainForm.InputString = textBox1.Text;
            this.Hide();
            
        }
    }
}
