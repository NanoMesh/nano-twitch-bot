﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanoTwitchBot
{

    class TwitchUserList
    {
        public TwitchUserList()
        {
            twitchUserList = new List<TwitchUser>();
        }

        public List<TwitchUser> twitchUserList { get; set; }

    }

    class TwitchUser
    {
        public string UserName { get; set;}
        public int NumberOfCookies { get; set; }
        public int NumberOfKappas { get; set; }
        public int NumberOfMessages { get; set; }
        public int Xp { get; set;}
        public int XpToLvlUp { get; set; }
        public int Lvl { get; set; }
        public TwitchUser()
        {
            UserName = "defaultName";
            NumberOfCookies = 0;
            NumberOfKappas = 0;
            NumberOfMessages = 0;
            Xp = 0;
            XpToLvlUp = 10;
            Lvl = 1;
        }
    }
}
