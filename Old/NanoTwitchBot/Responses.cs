﻿/*
 This is work of Josef "NanoMesh" Reichelt.
 Licensed under: https://creativecommons.org/licenses/by-nc-sa/4.0/
 More of my work can be found at https://www.artstation.com/artist/nanomesh and
 https://nanomesh.itch.io/.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace NanoTwitchBot
{
    class Responses
    {
        //Náhodná filler zpráva
        public string RandomFillMessage()
        {
            Datalist fillMsgs;
            string fillMsg = "Heh";
            if (File.Exists("Settings\\filler.json"))
            {
                fillMsg = File.ReadAllText("Settings\\filler.json");
                fillMsgs = new Datalist();
                fillMsgs = JsonConvert.DeserializeObject<Datalist>(fillMsg);
                Random rand = new Random();
                fillMsg = fillMsgs.dataList[rand.Next(fillMsgs.dataList.Count)];
            }
            return fillMsg;
        }
        //Načte seznam pozdravů a vybere náhodný
        public string RandomPozdrav()
        {
            Datalist pozdravy;
            string pozdrav = "Hoj";
            if (File.Exists("Settings\\pozdravy.json"))
            {
                pozdrav = File.ReadAllText("Settings\\pozdravy.json");
                pozdravy = new Datalist();
                pozdravy = JsonConvert.DeserializeObject<Datalist>(pozdrav);
                Random rand = new Random();
                pozdrav = pozdravy.dataList[rand.Next(pozdravy.dataList.Count)];

            }
            return pozdrav;
        }
        //Načte seznam pozdravů a vybere náhodný
        public string RandomSbohem()
        {
            Datalist sbohemy;
            string sbohem = "Hoj";
            if (File.Exists("Settings\\sbohem.json"))
            {
                sbohem = File.ReadAllText("Settings\\sbohem.json");
                sbohemy = new Datalist();
                sbohemy = JsonConvert.DeserializeObject<Datalist>(sbohem);
                Random rand = new Random();
                sbohem = sbohemy.dataList[rand.Next(sbohemy.dataList.Count)];

            }
            return sbohem;
        }
        //Hod mincí Panna, Orel
        public string CoinFlip(string message)
        {
            string vysledek = "";
            Random rnd = new Random();
            if (rnd.Next(0, 1) == 0)
            {
                if (message.ToLower().Contains("panna"))
                {
                    vysledek = "Padla Panna, vyhrál jsi.";
                }
                else
                {
                    vysledek = "Padla Panna, smůla.";
                }
            }
            else
            {
                if (message.ToLower().Contains("orel"))
                {
                    vysledek = "Padnul Orel, vyhrál jsi.";
                }
                else
                {
                    vysledek = "Padnul Orel, smůla.";
                }

            }
            if (!message.ToLower().Contains("panna") && !message.ToLower().Contains("orel"))
            {
                vysledek = "Zkus to znovu a tentokrat napis na co vsazis. Jestli Panna nebo Orel.";
            }
            return vysledek;
        }
        //Hodí kostkou 6, 12, 20
        public string DiceRoll(string message)
        {
            string vysledek = "";
            Random rnd = new Random();
            if (message.ToLower().Contains("6"))
            {

                    vysledek = $" hodil {rnd.Next(1,7)}";
            }
            if (message.ToLower().Contains("12"))
            {

                vysledek = $" hodil {rnd.Next(1, 13)}";
            }
            if (message.ToLower().Contains("20"))
            {

                vysledek = $" hodil {rnd.Next(1, 21)}";
            }
            if (!message.ToLower().Contains("6") && !message.ToLower().Contains("12") && !message.ToLower().Contains("20"))
            {
                vysledek = " Zkus to znovu a tentokrat napis jakou kostkou chceš hodit. Jestli 6,12, nebo 20";
            }
            return vysledek;
        }
        //Odpoví náhodnou emoticonou
        public string RandomEmote()
        {
            string vysledek = "";
            Random rnd = new Random();
            switch (rnd.Next(0, 7))
            {
                case 0:
                    vysledek = "Kappa";
                    break;
                case 1:
                    vysledek = "KappaClaus";
                    break;
                case 2:
                    vysledek = "KappaPride";
                    break;
                case 3:
                    vysledek = "KappaRoss";
                    break;
                case 4:
                    vysledek = "KappaWealth";
                    break;
                case 5:
                    vysledek = "Keepo";
                    break;
                case 6:
                    vysledek = "KAPOW";
                    break;
                default:
                    vysledek = "MrDestructoid";
                    break;

            }
            return vysledek;
        }
        //Random monster
        public string RandomMonster()
        {
            Datalist monsters;
            string monstrum = "Chybka v random monstru";
            if (File.Exists("Settings\\monsters.json"))
            {
                monstrum = File.ReadAllText("Settings\\monsters.json");
                monsters = new Datalist();
                monsters = JsonConvert.DeserializeObject<Datalist>(monstrum);
                Random rand = new Random();
                monstrum = monsters.dataList[rand.Next(monsters.dataList.Count)];

            }
            return monstrum;
        }

    }
}
