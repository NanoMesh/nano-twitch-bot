﻿/*
 This is work of Josef "NanoMesh" Reichelt.
 Licensed under: https://creativecommons.org/licenses/by-nc-sa/4.0/
 More of my work can be found at https://www.artstation.com/artist/nanomesh and
 https://nanomesh.itch.io/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NanoTwitchBot
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new NanoTwitchBotForm());
        }

    }
}
