﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanoTwitchBot
{
    class Datalist
    {

        public Datalist()
        {
            dataList = new List<string>();
        }

        public List<string> dataList { get; set; }
    }
    

    class ChannelList
    {

        public ChannelList()
        {
            channelList = new List<Channel>();
        }

        public List<Channel> channelList { get; set; }
    }

    class Channel
    {
        public string channelName { get; set; }
        public int channelId { get; set; }

    }
        
}