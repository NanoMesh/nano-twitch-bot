﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace NanoTwitchBot
{
    public class BotCore
    {
        //Connect vars
        TcpClient botTcpClient;
        StreamReader botReader;
        StreamWriter botWriter;

        //Bools
        bool joinedRoom;
        //Bot login info
        string botUserName;
        string botPassword;
        string botChannel;
        string chatMessagePrefix;

        //Lists vars
        Queue<string> messageQueue;
        DateTime messageQueueTime;
        DateTime lastRandomMsg;
        DateTime cookieDate;
        DateTime monsterDate;
        bool cookieActive;
        bool monsterActive;
        int fillerDellay;
        int cookieDellay;
        int monsterDellay;
        int monsterHP;
        DateTime UserListRefresher;
        DateTime boxClearer;

        string jsonReadString;
        string jsonWriteString;

        TwitchUserList listOfTwitchUsers;
        Responses responses;

        TextBox chatBox;
        Label chatUsersOnline;
        Label CookieTimerLabel;
        Label FillerTimerLabel;
        Label MonsterTimerLabel;
        int NumOfUsersOnline;
     
        public BotCore(TextBox chatBx,Label lbl,Label cookie,Label filler,Label mos)
        {
            chatBox = chatBx;
            chatUsersOnline = lbl;
            CookieTimerLabel = cookie;
            FillerTimerLabel = filler;
            MonsterTimerLabel = mos;
        }

        //Základní nastavení bota
        public void BotInit()
        {
            boxClearer = new DateTime();
            UserListRefresher = new DateTime();
            listOfTwitchUsers = new TwitchUserList();
            joinedRoom = false;
            responses = new Responses();
            messageQueue = new Queue<string>();
            //Connect
            botTcpClient = new TcpClient("irc.chat.twitch.tv", 6667);
            botReader = new StreamReader(botTcpClient.GetStream());
            botWriter = new StreamWriter(botTcpClient.GetStream());
            //Načíst věci ze souborů
            if (!Directory.Exists("Settings"))
            {
                Directory.CreateDirectory("Settings");
            }
            if (File.Exists("Settings/botUsername.json"))
            {
                botUserName = File.ReadAllText("Settings/botUsername.json");
            }
            else
            {
                File.WriteAllText("Settings/botUsername.json", "DEFAULT USER NAME PLEASE CHANGE");
                botUserName = "DEFAULTUSERNAMEPLEASECHANGE";
            }
            if (File.Exists("Settings/botpass.json"))
            {
                botPassword = File.ReadAllText("Settings/botpass.json");
            }
            else
            {
                File.WriteAllText("Settings/botpass.json", "DEFAULT PASSWORD PLEASE CHANGE");
                botPassword = "DEFAULTBOTPASSWORDPLEASECHANGE";
            }
            if (File.Exists("Settings/channel.json"))
            {
                botChannel = File.ReadAllText("Settings/channel.json");
            }
            else
            {
                File.WriteAllText("Settings/channel.json", "DEFAULT CHANNEL PLEASE CHANGE");
                botChannel = "DEFAULTBOTCHANNELPLEASECHANGE";
            }

            chatMessagePrefix = $":{botUserName}!{botUserName}@{botUserName}.tmi.twitch.tv PRIVMSG ";

            //Load Users
            if (File.Exists("Settings\\users.json"))
            {
            jsonReadString = File.ReadAllText("Settings\\users.json");
            listOfTwitchUsers = JsonConvert.DeserializeObject<TwitchUserList>(jsonReadString);
            }


            BotConnectToServer();
        }
        //Připojí se na IRC servery
        public void BotConnectToServer()
        {
            botWriter.WriteLine("PASS " + botPassword + Environment.NewLine + "NICK " + botUserName);
            botWriter.Flush();
            botWriter.WriteLine("CAP REQ :twitch.tv/membership");
            botWriter.Flush();
            joinedRoom = false;
        }
        //Připojí do kanálu
        public void BotConnectToRooms()
        {           
            //Reset dat pro casovace
            messageQueue.Clear();
            cookieActive = false;
            monsterActive = false;
            UserListRefresher = DateTime.Now;
            boxClearer = DateTime.Now;
            lastRandomMsg = DateTime.Now;
            cookieDate = DateTime.Now;
            messageQueueTime = DateTime.Now;
            monsterDate = DateTime.Now;
            //Zakladni random Susenky a filler
            Random rand = new Random();
            fillerDellay = rand.Next(10,40);
            cookieDellay = rand.Next(5,20);
            monsterDellay = rand.Next(10, 30);

            //Připojí se do místností
            botWriter.WriteLine($"JOIN #{botChannel}");
            botWriter.Flush();
            messageQueue.Enqueue($"#{botChannel} :/color OrangeRed");
            BotAddMessageToQueue($"Jsem Online! MrDestructoid Robotí revoluce začíná!");
                    
            
            joinedRoom = true;
        }
        //Loop
        public void BotLoop()
        {
                //Pokud není připojen tak připojit na server
                if (!botTcpClient.Connected)
                {
                    BotConnectToServer();
                }else
                {
                    //Pokud není v roomkách tak připojit do nich
                    if (!joinedRoom)
                    { 
                        BotConnectToRooms();
                    }
                }
                if (joinedRoom)
                {
                    BotReadMessage();
                    BotSendMessageFromQueue();
                    BotRandomFillerMsg();
                    cookieSend();
                    monsterSend();
                    TimeSpan cookieSpan = DateTime.Now - cookieDate;
                    TimeSpan fillerSpan = DateTime.Now - lastRandomMsg;
                    TimeSpan monsterSpan = DateTime.Now - monsterDate;
                    CookieTimerLabel.Text = $"Cookie in: {(cookieSpan - TimeSpan.FromMinutes(cookieDellay)).Minutes}m {(cookieSpan - TimeSpan.FromMinutes(cookieDellay)).Seconds}s";

                    FillerTimerLabel.Text = $"Filler in: {(fillerSpan - TimeSpan.FromMinutes(fillerDellay)).Minutes}m {(fillerSpan - TimeSpan.FromMinutes(fillerDellay)).Seconds}s";

                    MonsterTimerLabel.Text = $"Monster in: {(monsterSpan - TimeSpan.FromMinutes(monsterDellay)).Minutes}m {(monsterSpan - TimeSpan.FromMinutes(monsterDellay)).Seconds}s";

                if (DateTime.Now - UserListRefresher > TimeSpan.FromMinutes(10))
                    {
                    runSaveUsers();
                    }

                }
                if(DateTime.Now - boxClearer > TimeSpan.FromHours(2))
                {
                    chatBox.Clear();
                    boxClearer = DateTime.Now;
                }
            

        }
        //Čtení zpráv
        public void BotReadMessage()
        {
            if (botTcpClient.Available > 0 || botReader.Peek() >= 0)
            {
                //Jakkákoliv zpráva
                var readChatMessage = botReader.ReadLine();
                var dvojtecka = readChatMessage.IndexOf(":", 1);
                //DebugLog
                File.AppendAllText($"Logs\\Debug {DateTime.Today.Day}-{DateTime.Today.Month}-{DateTime.Today.Year}.txt", $"\r\n {DateTime.Now} | Chat message | {readChatMessage}");
                if (dvojtecka > 0)
                {
                    var prikaz = readChatMessage.Substring(1, dvojtecka);
                    
                    //Pokud se jedna o private message
                    if (prikaz.Contains("PRIVMSG"))
                    {
                        var userPrefix = prikaz.IndexOf("!");
                        if (userPrefix > 0)
                        {
                            //Jméno uživatele
                            var chatUser = prikaz.Substring(0, userPrefix);
                            //Zpráva co napsal
                            var chatMsg = readChatMessage.Substring(dvojtecka + 1);

                            CheckIfUserExists(chatUser);
                            BotRecievedMessage(chatUser, chatMsg);
                            BotLog(chatUser, chatMsg);
                        }
                    }
                }
                    //TwitchPing
                    if (readChatMessage.Contains("PING"))
                    {
                        botWriter.WriteLine("PONG :tmi.twitch.tv");
                        botWriter.Flush();
                        File.AppendAllText($"Logs\\Debug {DateTime.Today.Day}-{DateTime.Today.Month}-{DateTime.Today.Year}.txt", $"\r\n {DateTime.Now} | PONG :tmi.twitch.tv");
                    }
                    //User se připojil
                    if (readChatMessage.Contains("JOIN"))
                    {
                        var userPrefix = readChatMessage.IndexOf("!");
                        if (userPrefix > 0)
                        {
                            //Jméno uživatele
                            var chatUser = readChatMessage.Substring(1, userPrefix-1);
                            if(chatUser.ToLower() != botUserName)
                            {

                                BotAddMessageToQueue($" {responses.RandomPozdrav()} {chatUser} ");
                                File.AppendAllText($"Logs\\Debug {DateTime.Today.Day}-{DateTime.Today.Month}-{DateTime.Today.Year}.txt", $"\r\n {DateTime.Now} | User {chatUser} se pripojil");
                                NumOfUsersOnline++;
                                chatUsersOnline.Text = $"Online: {NumOfUsersOnline.ToString()}";
                            
                            }
                        }
                    }
                    //User se odpojil
                    if (readChatMessage.Contains("PART"))
                    {
                        var userPrefix = readChatMessage.IndexOf("!");
                        if (userPrefix > 0)
                        {
                            //Jméno uživatele
                            var chatUser = readChatMessage.Substring(1, userPrefix - 1);
                            if (chatUser.ToLower() != botUserName)
                            {
                                chatBox.AppendText($"\r\n {chatUser} se odpojil");
                                BotLog(chatUser," se odpojil");
                                File.AppendAllText($"Logs\\Debug {DateTime.Today.Day}-{DateTime.Today.Month}-{DateTime.Today.Year}.txt", $"\r\n {DateTime.Now} | User {chatUser} se odpojil");
                                NumOfUsersOnline--;
                                chatUsersOnline.Text = $"Online: {NumOfUsersOnline.ToString()}";
                        }
                        }
                   }
                    

            }
        }
        //Pošle zprávu do chatu
        public void BotSendMessageFromQueue()
        {
            if (DateTime.Now - messageQueueTime > TimeSpan.FromSeconds(1))
            {
                if (messageQueue.Count > 0)
                {
                        var message = messageQueue.Dequeue();
                        botWriter.WriteLine($"{chatMessagePrefix}{message}");
                        chatBox.AppendText($"\r\nBot odpovedel : {message}");
                        BotLog(botUserName, message);
                        botWriter.Flush();
                        messageQueueTime = DateTime.Now;
                }
            }
        }
        //Přidá zprávu do fronty kanálu ze kterého přišla
        public void BotAddMessageToQueue(string msgToQueue)
        {
            if (messageQueue.Count < 15)
            {
                messageQueue.Enqueue($"#{botChannel} :/me [◔□◔]  {msgToQueue}");
                
            }
        }        
        //Zapise zpravu do logu
        public void BotLog(string chatSpeaker, string chatMessage)
        {
            //Logovani
            if (!Directory.Exists("Logs")){Directory.CreateDirectory("Logs");}
            if (!Directory.Exists($"Logs\\{botChannel}")){Directory.CreateDirectory($"Logs\\{botChannel}");}
            File.AppendAllText($"Logs\\{botChannel}\\Log {DateTime.Today.Day}-{DateTime.Today.Month}-{DateTime.Today.Year}.txt", $"\r\n{DateTime.Now} | Channel:{botChannel} | User:{chatSpeaker} | Msg:{chatMessage}");
        }
        //Exit
        public async Task BotQuit()
        {
            botWriter.WriteLine($"PART #{botChannel}");
            botWriter.Flush();
            await Task.Delay(100);
        }

        
        //PŘÍKAZY!!!!!!!!!
        //
        //Přijatá zpráva z chatu
        public void BotRecievedMessage(string chatSpeaker, string chatMessage)
        {
            chatBox.AppendText($"\r\n{DateTime.Now} | {botChannel} | {chatSpeaker} | {chatMessage}");


                if (chatMessage.ToLower().StartsWith("-help"))
                {
                    BotAddMessageToQueue($"TwitchRPG | -flip | -unflip | -nom (pro zpapání NomNom ) | -topnom | -info | -stats | -fight(pro boj s monstrem) | TwitchRPG");
                }
                /*if (chatMessage.ToLower().StartsWith("kappa"))
                {
                    BotAddMessageToQueue($"{responses.RandomEmote()}");
                    AddNumOfKappas(chatSpeaker);
                }*/
                //Top 3 žrouti
                if(chatMessage.ToLower().StartsWith("-topnom")) 
                {
                    var res = listOfTwitchUsers.twitchUserList.OrderByDescending(u => u.NumberOfCookies).Take(3).ToList();
                    BotAddMessageToQueue($"Top žrouti {res[0].UserName} s {res[0].NumberOfCookies} NomNom || {res[1].UserName} s {res[1].NumberOfCookies} NomNom || {res[2].UserName} s {res[2].NumberOfCookies} NomNom ");
                 }
                if (chatMessage.ToLower().StartsWith("42"))
                {
                    BotAddMessageToQueue($"To je odpověď, ale jak zní otázka?");
                }
                if (chatMessage.ToLower().StartsWith("-flip"))
                {
                    BotAddMessageToQueue($"(╯°□°）╯︵ ┻━┻");
                }
                if (chatMessage.ToLower().StartsWith("-unflip"))
                {
                    BotAddMessageToQueue($"┬─┬﻿ ノ( ゜-゜ノ)");
                }
                if (chatMessage.ToLower().StartsWith("jak se mas")|| chatMessage.ToLower().StartsWith("jak se máš")||chatMessage.ToLower().StartsWith("jak se más") || chatMessage.ToLower().StartsWith("jak to de") || chatMessage.ToLower().StartsWith("jak se daří") || chatMessage.ToLower().StartsWith("jak se dari"))
                {
                    BotAddMessageToQueue($" Mám se dobře, díky za optání {chatSpeaker}");
                }
                /*if (chatMessage.ToLower().StartsWith("-coinflip"))
                {
                    BotAddMessageToQueue($"{responses.CoinFlip(chatMessage)}{chatSpeaker}");
                }
                if (chatMessage.ToLower().StartsWith("-dice"))
                {
                    BotAddMessageToQueue($"{chatSpeaker}{responses.DiceRoll(chatMessage)}");
                }*/
                if (chatMessage.ToLower().StartsWith("-info"))
                {
                    BotAddMessageToQueue($"Ja jsem NanoBotak 5000, byl jsem sestrojen Nanomeshem a jeho další tvorbu naleznete na https://www.artstation.com/artist/nanomesh a https://nanomesh.itch.io/ .Výpis mých příkazů získáte -help .Snad si mě oblíbíte a prosím nesnažte se mě rozbít MrDestructoid");
                }
                if (chatMessage.ToLower().StartsWith("-stats"))
                {
                GetStats(chatSpeaker);
                }
                if (chatMessage.ToLower().StartsWith("-nom"))
                {
                    cookieCheck(chatSpeaker);
                    
                }
                if (chatMessage.ToLower()== "-fight")
                {
                    Random rand = new Random();
                    var dmg = rand.Next(10,20);
                    monsterCheck(chatSpeaker,dmg);
                }


        }
        
        //Vrátí statistiky uživatele.
        public void GetStats(string userName)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x=>x.UserName==userName);
            BotAddMessageToQueue($"{twitchUser.UserName}  | {twitchUser.NumberOfMessages} Zpráv  | {twitchUser.NumberOfCookies} NomNom  | TwitchRPG LVL: {twitchUser.Lvl} | XP: {twitchUser.Xp} | Xp do dalšího levelu {twitchUser.XpToLvlUp - twitchUser.Xp} ");
        }
        //
        //
        //
        //User Management
        public void AddNumOfMessages(string userName)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x => x.UserName == userName);
            if (twitchUser != null)
            {
                twitchUser.NumberOfMessages += 1;
                AddXp(twitchUser.UserName, 1, 2);
            }
        }
        public void AddNumOfcookies(string userName)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x => x.UserName == userName);
            if (twitchUser != null)
            {
                twitchUser.NumberOfCookies += 1;
                AddXp(twitchUser.UserName, 3,5);
            }
        }
        public void AddXp(string userName, int randRange1, int randRange2)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x => x.UserName == userName);
            if (twitchUser != null)
            {
                if(twitchUser.Lvl == 1) { twitchUser.XpToLvlUp = 10; }
                Random rand = new Random();
                twitchUser.Xp += rand.Next(randRange1, randRange2);
                if (twitchUser.Xp > twitchUser.XpToLvlUp)
                {
                    twitchUser.Lvl++;
                    twitchUser.XpToLvlUp = twitchUser.XpToLvlUp * 2;
                    BotAddMessageToQueue($" TwitchRPG {twitchUser.UserName} je nyní LVL: {twitchUser.Lvl} TwitchRPG ");

                }
            }
        }
        public void AddNumOfKappas(string userName)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x => x.UserName == userName);
            if (twitchUser != null)
            {
                twitchUser.NumberOfKappas += 1;
                AddXp(twitchUser.UserName, 2, 3);
            }
        }
        public void CheckIfUserExists(string userName)
        {
            TwitchUser twitchUser = new TwitchUser();
            twitchUser = listOfTwitchUsers.twitchUserList.Find(x => x.UserName == userName);

            if (twitchUser==null)
            {
                twitchUser = new TwitchUser();
                twitchUser.UserName = userName;
                listOfTwitchUsers.twitchUserList.Add(twitchUser);
            }
            AddNumOfMessages(userName);
        }
        //Zapíše a opět načte seznam uživatelů
        public async void runSaveUsers()
        {
            await RefreshUsers();
        }
        public async Task RefreshUsers()
        {
                jsonWriteString = JsonConvert.SerializeObject(listOfTwitchUsers);
                File.WriteAllText("Settings\\users.json", jsonWriteString);
                await Task.Delay(250);
                jsonReadString = File.ReadAllText("Settings\\users.json");
                listOfTwitchUsers = JsonConvert.DeserializeObject<TwitchUserList>(jsonReadString);
                jsonWriteString = " ";
                jsonReadString = " ";
                UserListRefresher = DateTime.Now;
                await Task.Delay(250);
        }
        //Pošle náhodný filler
        public void BotRandomFillerMsg()
        {
            if (DateTime.Now - lastRandomMsg > TimeSpan.FromMinutes(fillerDellay))
                {
                    BotAddMessageToQueue($"{responses.RandomFillMessage()}");
                    lastRandomMsg = DateTime.Now;
                    Random rand = new Random();
                    fillerDellay = rand.Next(5, 15);
                }
            
        }
        //CookieEngine
        public void cookieSend()
        {

                if (DateTime.Now - cookieDate > TimeSpan.FromMinutes(cookieDellay))
                {
                    if(cookieActive == false)
                    {
                    BotAddMessageToQueue($"NomNom");
                    cookieActive = true;
                    }
                }
                if (DateTime.Now - cookieDate > TimeSpan.FromMinutes(cookieDellay+5))
                {
                    if (cookieActive == true)
                    {
                        cookieCheck(botUserName);

                    }
                }
            
        }
        public void cookieCheck(string user)
        {

            if (cookieActive == true)
            {
                BotAddMessageToQueue($"{user} snědl NomNom ");
                cookieDate = DateTime.Now;
                Random rand = new Random();
                cookieDellay = rand.Next(5, 20);
                cookieActive = false;
                AddNumOfcookies(user);
            }
            else
            {
                BotAddMessageToQueue($"Žádný sušenky tu nejsou.");
            }
        }
        public void SendCookieNow()
        {
            if (cookieActive == false)
            {

                cookieDate = DateTime.Now;
                Random rand = new Random();
                cookieDellay = rand.Next(5, 20);
                BotAddMessageToQueue($"NomNom");
                cookieActive = true;
            }
        }

        //Monster Engine
        public void monsterSend()
        {

            if (DateTime.Now - monsterDate > TimeSpan.FromMinutes(monsterDellay))
            {
                if (monsterActive == false)
                {
                    Random rand = new Random();
                    monsterHP = rand.Next(50, 200);
                    BotAddMessageToQueue($"{responses.RandomMonster()} a má {monsterHP}HP");
                    monsterActive = true;
                }
            }
            if (DateTime.Now - monsterDate > TimeSpan.FromMinutes(monsterDellay + 10))
            {
                if (monsterActive == true)
                {
                    BotAddMessageToQueue($"Bohužel monstrum vyhrálo a všechny zde porazilo. Snad se ho podaří porazit příště.");
                    monsterDate = DateTime.Now;
                    Random rand = new Random();
                    monsterDellay = rand.Next(10, 20);
                    monsterActive = false;
                }
            }

        }
        public void monsterCheck(string user,int dmg)
        {

            if (monsterActive == true)
            {
                if(monsterHP > 0)
                {
                    monsterHP -= dmg;
                    if(monsterHP - dmg <= 0)
                    {
                        BotAddMessageToQueue($"Monstrum bylo úspěšně poraženo. Poslední ránu mu zasadil {user} TwitchRPG");
                        AddXp(user, 15, 30);
                        monsterDate = DateTime.Now;
                        Random rand = new Random();
                        monsterDellay = rand.Next(10, 20);
                        monsterActive = false;
                    }
                    else
                    {
                        AddXp(user, 5, 15);
                        BotAddMessageToQueue($"Monstrum bylo zasaženo. Zbývá mu {monsterHP} HP TwitchRPG ");
                    }
                }
            }
            else
            {
                BotAddMessageToQueue($"Žádný monstra tu nejsou. {user}");
            }
        }

        public void SendMonsterNow()
        {
            if (monsterActive == false)
            {
                monsterDate = DateTime.Now;
                Random rand = new Random();
                monsterDellay = rand.Next(10, 20);
                monsterHP = rand.Next(50, 200);
                BotAddMessageToQueue($"{responses.RandomMonster()} a má {monsterHP}HP");
                monsterActive = true;
            }
        }

    }
}
