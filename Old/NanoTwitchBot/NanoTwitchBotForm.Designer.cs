﻿namespace NanoTwitchBot
{
    partial class NanoTwitchBotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NanoTwitchBotForm));
            this.botTimer = new System.Windows.Forms.Timer(this.components);
            this.chatBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chatOnlineUsers = new System.Windows.Forms.Label();
            this.cookieTimer = new System.Windows.Forms.Label();
            this.fillerTimer = new System.Windows.Forms.Label();
            this.sendCookieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMonsterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monsterLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // botTimer
            // 
            this.botTimer.Enabled = true;
            this.botTimer.Tick += new System.EventHandler(this.botTimer_Tick);
            // 
            // chatBox
            // 
            this.chatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(31)))), ((int)(((byte)(75)))));
            this.chatBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chatBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.chatBox, "chatBox");
            this.chatBox.ForeColor = System.Drawing.Color.Silver;
            this.chatBox.Name = "chatBox";
            this.chatBox.ReadOnly = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Name = "label1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(31)))), ((int)(((byte)(75)))));
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveUsersToolStripMenuItem,
            this.sendCookieToolStripMenuItem,
            this.sendMonsterToolStripMenuItem,
            this.fileToolStripMenuItem});
            this.menuStrip1.Name = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(31)))), ((int)(((byte)(75)))));
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // saveUsersToolStripMenuItem
            // 
            resources.ApplyResources(this.saveUsersToolStripMenuItem, "saveUsersToolStripMenuItem");
            this.saveUsersToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.saveUsersToolStripMenuItem.Name = "saveUsersToolStripMenuItem";
            this.saveUsersToolStripMenuItem.Click += new System.EventHandler(this.saveUsersToolStripMenuItem_Click);
            // 
            // chatOnlineUsers
            // 
            resources.ApplyResources(this.chatOnlineUsers, "chatOnlineUsers");
            this.chatOnlineUsers.Name = "chatOnlineUsers";
            // 
            // cookieTimer
            // 
            resources.ApplyResources(this.cookieTimer, "cookieTimer");
            this.cookieTimer.Name = "cookieTimer";
            // 
            // fillerTimer
            // 
            resources.ApplyResources(this.fillerTimer, "fillerTimer");
            this.fillerTimer.Name = "fillerTimer";
            // 
            // sendCookieToolStripMenuItem
            // 
            resources.ApplyResources(this.sendCookieToolStripMenuItem, "sendCookieToolStripMenuItem");
            this.sendCookieToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.sendCookieToolStripMenuItem.Name = "sendCookieToolStripMenuItem";
            this.sendCookieToolStripMenuItem.Click += new System.EventHandler(this.sendCookieToolStripMenuItem_Click);
            // 
            // sendMonsterToolStripMenuItem
            // 
            resources.ApplyResources(this.sendMonsterToolStripMenuItem, "sendMonsterToolStripMenuItem");
            this.sendMonsterToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.sendMonsterToolStripMenuItem.Name = "sendMonsterToolStripMenuItem";
            this.sendMonsterToolStripMenuItem.Click += new System.EventHandler(this.sendMonsterToolStripMenuItem_Click);
            // 
            // monsterLabel
            // 
            resources.ApplyResources(this.monsterLabel, "monsterLabel");
            this.monsterLabel.Name = "monsterLabel";
            // 
            // NanoTwitchBotForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(30)))), ((int)(((byte)(51)))));
            this.Controls.Add(this.monsterLabel);
            this.Controls.Add(this.fillerTimer);
            this.Controls.Add(this.cookieTimer);
            this.Controls.Add(this.chatOnlineUsers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chatBox);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.DarkGray;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "NanoTwitchBotForm";
            this.Load += new System.EventHandler(this.NanoTwitchBotForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer botTimer;
        private System.Windows.Forms.TextBox chatBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Label chatOnlineUsers;
        private System.Windows.Forms.Label cookieTimer;
        private System.Windows.Forms.Label fillerTimer;
        private System.Windows.Forms.ToolStripMenuItem saveUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendCookieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMonsterToolStripMenuItem;
        private System.Windows.Forms.Label monsterLabel;
    }
}

