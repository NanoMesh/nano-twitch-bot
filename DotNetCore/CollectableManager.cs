using System;
using System.Threading.Tasks;

namespace NanoTwitchBot
{
public class CollectableManager{

    public Core BotCore;
    public CollectableManager(Core ct){
        BotCore = ct;
    }

    public async void CollectableSender(Collectable coll){
        //Not active, sending
        if(DateTime.Now - coll.CollectableLastTimeSent > TimeSpan.FromMinutes(coll.CollectableDelay) && coll.CollectableActive == false){
            await SendCollectable(coll);

        }
        //Not Collected
        if(DateTime.Now - coll.CollectableLastTimeSent > TimeSpan.FromMinutes(coll.CollectableDelay+5) && coll.CollectableActive == true){
            if(coll.CollectableName == "NomNom"){
            await CollectCollectable(coll,BotCore.BotSettings.BotUserName,"Snědl sušenku");
            }
            if(coll.CollectableName == "TBTacoLeft TBCheesePull TBTacoRight"){
                await CollectCollectable(coll,BotCore.BotSettings.BotUserName,"Snědl Taco");
            }
            if(coll.CollectableName == "SMOrc"){
                await CollectCollectable(coll,BotCore.BotSettings.BotUserName,"Zabil Orka");
            }
        }
    }

    public async Task SendCollectable(Collectable coll){
        coll.CollectableActive = true;
        await BotCore.BotCoreTasks.SendMessage(coll.CollectableName);
        await Task.Delay(10);
    }
    public async Task CollectCollectable(Collectable coll,string userName,string CollectMessage){
        coll.CollectableActive = false;
        coll.CollectableLastTimeSent = DateTime.Now;
        Random rand = new Random();
        coll.CollectableDelay = rand.Next(10,45);
        await BotCore.BotCoreTasks.SendMessage($"{userName} {CollectMessage}");
    }
}
}
