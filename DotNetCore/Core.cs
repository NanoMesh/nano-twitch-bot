﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NanoTwitchBot
{
  public class Core
  {
        public static void Main(string[] args){
            new Core().BotInitialization().GetAwaiter().GetResult();
        }

    //┌───────────┐
    //│ Variables │
    //└───────────┘
    //
    //Connect vars
    //
    public TcpClient BotTcpClient;
    public StreamReader BotStreamReader;
    public StreamWriter BotStreamWriter;
    public string BotMessagePrefix;
    public bool BotJoinedRooms = false;
    public bool BotLoggedIn = false;

    //
    //Time vars
    //
    public Timer BotLooperTimer;         //Timer for a bot loop used for timed saves, setting of a game etc..
    public DateTime SaveTimer;
    public DateTime RandomFillerTimer;
    public int RandomFillerDelay;

    //
    //Class References
    //
    public CoreTasks BotCoreTasks;
    public Settings BotSettings = new Settings();
    public CollectableManager BotCollectableManager;
    //
    //Misc Vars
    //
    public Dictionary<string,User> DictionaryOfUsers = new Dictionary<string,User>();
    public List<string> BotLogMessageList = new List<string>();
    public List<Collectable> CollectablesList = new List<Collectable>();
    //┌────────────────────────────────────────────────────────────────────┐
    //│ Write to console with colored prefix, add it to a list for logging │
    //└────────────────────────────────────────────────────────────────────┘
    public void PrintUsingColor(ConsoleColor prefixColor, string prefix, string message){
        ConsoleColor origColor = Console.ForegroundColor;   //Save the original Color
        Console.ForegroundColor = ConsoleColor.Cyan;        //Set Color of Date to cyan
        Console.Write($"{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}");                   //Start the message with a Time
        Console.ForegroundColor = prefixColor;              //Set Color of the prefix to parsed parameter
        Console.Write($" │ {prefix} │ ");                   //Add a prefix to the message
        Console.ForegroundColor = origColor;                //Reset color to original
        Console.Write($"{message}{Environment.NewLine}");
        BotLogMessageList.Add($"{DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year} {DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second} │ {prefix} │ {message}"); //Add message to list for saving
            
    }
    //┌────────────────────┐
    //│ Bot Initialization │
    //└────────────────────┘
    public async Task BotInitialization(){
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        Console.OutputEncoding = Encoding.GetEncoding("windows-1250");
        PrintUsingColor(ConsoleColor.Red, "[O_O]", "BOT STARTING");
        BotCoreTasks = new CoreTasks(this);
        BotCollectableManager = new CollectableManager(this);
        BotSettings = await BotCoreTasks.LoadSettings();
        DictionaryOfUsers = await BotCoreTasks.LoadUsers();
        BotMessagePrefix = $":{BotSettings.BotUserName}!{BotSettings.BotUserName}@{BotSettings.BotUserName}.tmi.twitch.tv PRIVMSG ";
        BotTcpClient = new TcpClient();
        await BotTcpClient.ConnectAsync("irc.chat.twitch.tv", 6667);
        BotStreamReader = new StreamReader(BotTcpClient.GetStream());
        BotStreamWriter = new StreamWriter(BotTcpClient.GetStream());
        //
        //Create Collectables
        //
        Random rand = new Random();
        CollectablesList.Add(new Collectable(){
            CollectableName = "NomNom",
            CollectableDelay = rand.Next(15,45)
        });
        CollectablesList.Add(new Collectable(){
            CollectableName = "TBTacoLeft TBCheesePull TBTacoRight",
            CollectableDelay = rand.Next(15,45)
            });
        CollectablesList.Add(new Collectable(){
            CollectableName = "SMOrc",
            CollectableDelay = rand.Next(15,45)
            });

        //
        //Setup and start the bot loop
        //
        PrintUsingColor(ConsoleColor.Green, "[O_O]", $"STARTING LOOP");
        SaveTimer = DateTime.Now;
        RandomFillerTimer = DateTime.Now;
       
        RandomFillerDelay = rand.Next(15,45);
        BotLooperTimer = new Timer(new TimerCallback(BotLoop),null,100,100);
        await Task.Delay(-1);
    }
    //┌──────────┐
    //│ Bot Loop │
    //└──────────┘
    public async void BotLoop(Object stateinfo){
        if(BotLoggedIn == false){
            BotLoggedIn = true;
            await BotCoreTasks.ConnectToIrc();
            PrintUsingColor(ConsoleColor.Green, "[O_O]", "BOT ONLINE");
            await BotCoreTasks.SendMessage("Bot Online! MrDestructoid");
        }
        if(BotJoinedRooms == true){
            ReadMessage();
            //
            //Save Chat and Users every 15minutes
            //
            if(DateTime.Now - SaveTimer > TimeSpan.FromMinutes(15)){
                SaveTimer = DateTime.Now;
                await BotCoreTasks.SaveData();
            }
            //
            //Send random message
            //
            if(DateTime.Now - RandomFillerTimer > TimeSpan.FromMinutes(RandomFillerDelay)){
                RandomFillerTimer = DateTime.Now;
                Random rand = new Random();
                RandomFillerDelay = rand.Next(10,45);
                await BotCoreTasks.SendMessage(await BotCoreTasks.RandomFiller());
            }
            //
            //Collectables
            //
            foreach(Collectable coll in CollectablesList){
                BotCollectableManager.CollectableSender(coll);
            }

        }
        
    }

    public async void ReadMessage(){
        //
        //If any data is available
        //
        if(BotTcpClient.Available > 0 || BotStreamReader.Peek() >= 0)
        {
            var RawMessage = BotStreamReader.ReadLine();
            PrintUsingColor(ConsoleColor.Blue,"RAW",RawMessage);
            var ColonIndex = RawMessage.IndexOf(':',1);
            //
            //If Raw contains a message
            //
            if(ColonIndex > 0){
                var command = RawMessage.Substring(1,ColonIndex);
                //
                //If is a message from chat
                //
                if(command.Contains("PRIVMSG")){
                    var IndexOfUser = command.IndexOf('!');
                    if(IndexOfUser > 0){
                        var ChatUserName = command.Substring(0,IndexOfUser);
                        var ChatMessage = RawMessage.Substring(ColonIndex+1);
                        PrintUsingColor(ConsoleColor.Green,"Message",$"{ChatUserName}: {ChatMessage}");
                        await BotCoreTasks.CheckForUser(ChatUserName);
                        Commands(ChatMessage,ChatUserName);
                        await BotCoreTasks.AddXpToUser(ChatUserName,10);
                    }
                }
            }
            //
            //Respond to Twitch Ping to prevent disconnecting
            //
            if(RawMessage.Contains("PING")){
                await BotStreamWriter.WriteLineAsync("PONG :tmi.twitch.tv");
                BotStreamWriter.Flush();
                PrintUsingColor(ConsoleColor.Red,"[O_O]","RESPONDED WITH PONG");
            }
            //
            //User Joined the chatroom
            //
            if (RawMessage.Contains("JOIN"))
            {
                var UserIndex = RawMessage.IndexOf('!');
                if(UserIndex > 0){
                    var UserName = RawMessage.Substring(1,UserIndex-1);
                    if(!UserName.ToLower().Contains(BotSettings.BotUserName)){
                        User usr;
                        if(DictionaryOfUsers.TryGetValue(UserName,out usr)){
                        await BotCoreTasks.SendMessage($"{await BotCoreTasks.RandomGreetings()} {UserName} bojovníku s LVL {usr.UserLVL}");    
                        }else{
                        await BotCoreTasks.SendMessage($"{await BotCoreTasks.RandomGreetings()} {UserName}");
                        }
                        PrintUsingColor(ConsoleColor.DarkCyan,"Joined",UserName);
                    }
                }
            }
            //
            //User left the chatroom
            //
            if (RawMessage.Contains("PART"))
            {
                var UserIndex = RawMessage.IndexOf('!');
                if(UserIndex > 0){
                    var UserName = RawMessage.Substring(1,UserIndex-1);
                    if(!UserName.ToLower().Contains(BotSettings.BotUserName)){
                        await BotCoreTasks.SendMessage($"{UserName} nás opustil.");
                        PrintUsingColor(ConsoleColor.DarkCyan,"Left",UserName);
                        if(File.Exists("Data\\GoodbyeMessage.txt")){
                        var byeMsg = File.ReadAllText("Data\\GoodbyeMessage.txt");
                        await BotCoreTasks.SendWhisperMessage(byeMsg,UserName);
                        }
                    }
                }
            }
        }
    }

    public async void Commands(string Message, string userName){
        if(Message == "-help"){
            await BotCoreTasks.SendWhisperMessage($"|| Zobrazí staty -stats | NomNom -nom | TBTacoLeft TBCheesePull TBTacoRight -taco | SMOrc -fight ||",userName);
        }
        if(Message == "42"){
            await BotCoreTasks.SendMessage("To je odpověď, ale jak zní otázka?");
        }
        if(Message.Contains("(╯°□°）╯︵ ┻━┻") || Message.Contains("┻━┻")){
            await BotCoreTasks.SendMessage("┬─┬ ノ( ゜-゜ノ)");
        }
        if(Message == "-stats"){
            User usr;
            if(DictionaryOfUsers.TryGetValue(userName.ToLower(),out usr)){
            await BotCoreTasks.SendMessage($"{usr.UserName} TwitchRPG | LVL: {usr.UserLVL} | Xp: {usr.UserXpToLvlUp-usr.UserXp} | Xp to next level: {usr.UserXpToLvlUp} | {usr.NumberOfMessages} Msgs | {usr.NumberOfCookies} NomNom | {usr.NumberOfTacos} TBTacoLeft TBCheesePull TBTacoRight | {usr.NumberOfOrcs} SMOrc");
            }
        }
        //
        //Collecting
        //
        foreach(Collectable coll in CollectablesList){

                if(Message == "-nom" && coll.CollectableName == "NomNom"){
                    if(coll.CollectableActive == true){
                        await BotCollectableManager.CollectCollectable(coll,userName,"Snědl sušenku");
                        User usr;
                        if(DictionaryOfUsers.TryGetValue(userName.ToLower(),out usr)){
                            usr.NumberOfCookies++;}
                        await BotCoreTasks.AddXpToUser(userName,25);
                        break;
                    }else{
                        await BotCoreTasks.SendMessage("Žádné sušenku tu nejsou");
                        break;
                    }
                }
                if(Message == "-taco" && coll.CollectableName == "TBTacoLeft TBCheesePull TBTacoRight"){
                    if(coll.CollectableActive == true){
                        User usr;
                        if(DictionaryOfUsers.TryGetValue(userName.ToLower(),out usr)){
                            usr.NumberOfTacos++;}
                        await BotCollectableManager.CollectCollectable(coll,userName,"Snědl Taco");
                        await BotCoreTasks.AddXpToUser(userName,25);

                        break;
                    }else{
                        await BotCoreTasks.SendMessage("Žádné Taca tu nejsou");
                        break;
                    }
                }
                 if(Message == "-fight" && coll.CollectableName == "SMOrc"){
                    if(coll.CollectableActive == true){
                        User usr;
                        if(DictionaryOfUsers.TryGetValue(userName.ToLower(),out usr)){
                           usr.NumberOfOrcs++;}
                        await BotCollectableManager.CollectCollectable(coll,userName,"zabil Orka");
                        await BotCoreTasks.AddXpToUser(userName,25);
                        break;
                    }else{
                        await BotCoreTasks.SendMessage("Žádní Orci tu nejsou");
                        break;
                    }
                }
        }
    }
  }
}
