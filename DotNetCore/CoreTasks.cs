using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using System.Net.Sockets;

namespace NanoTwitchBot
{
  public class CoreTasks
  {
    public Core BotCore;
    public CoreTasks(Core ct){
          BotCore = ct;
        }
    public async Task<Settings> LoadSettings(){
        BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "LOADING SETTINGS");
        //
        //Check if Folder structure exists
        //
        if (!Directory.Exists("Data")) { Directory.CreateDirectory("Data"); }
        if (!Directory.Exists("Logs")) { Directory.CreateDirectory("Logs"); }
        
        Settings tempSet = new Settings();
        //
        //Tries to load data from Settings.json file
        //
        if (File.Exists($"Data\\Settings.json"))
        {
        string jason = File.ReadAllText("Data\\Settings.json");
        tempSet = JsonConvert.DeserializeObject<Settings>(jason);
        BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "SETTINGS SUCCESSFULLY LOADED");
        }
        //
        //Settings.json file not found, create new one with deffault settings.
        //
        else
        {
            BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "SETTINGS FILE NOT FOUND, CREATING");
            string jason = JsonConvert.SerializeObject(tempSet);
            File.WriteAllText("Data\\Settings.json", jason);
        }
        await Task.Delay(100);
        return tempSet;
    }

    public async Task<Dictionary<string,User>> LoadUsers(){
        Dictionary<string,User> tempDic = new Dictionary<string,User>();
        //
        //Tries to load data from Users.json
        //
        if(File.Exists("Data\\Users.json")){
        string jason = File.ReadAllText("Data\\Users.json");
        tempDic = JsonConvert.DeserializeObject<Dictionary<string,User>>(jason);
        BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "USERS SUCCESSFULLY LOADED");
        }
        //
        //Users.json file not found, create new one with deffault settings.
        //
        else{
            BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "USERS FILE NOT FOUND, CREATING");
            tempDic.Add("U1",new User());
            tempDic.Add("U2",new User());
            string jason = JsonConvert.SerializeObject(tempDic);
            File.WriteAllText("Data\\Users.json", jason);
        }
        await Task.Delay(1);
        return tempDic;
    }
    public async Task ConnectToIrc(){
        //
        //Login Twitch IRC
        //
        if(BotCore.BotTcpClient.Connected == true){

        BotCore.PrintUsingColor(ConsoleColor.Red, "[O_O]", "BOT CONNECTING  ");
        
        await BotCore.BotStreamWriter.WriteLineAsync("PASS " + BotCore.BotSettings.BotPassword);
        BotCore.BotStreamWriter.Flush();
        await BotCore.BotStreamWriter.WriteLineAsync("NICK " + BotCore.BotSettings.BotUserName);
        BotCore.BotStreamWriter.Flush();
        BotCore.PrintUsingColor(ConsoleColor.Red, "[O_O]", "BOT CONNECTED");
        BotCore.BotStreamWriter.WriteLine("CAP REQ :twitch.tv/membership");
        BotCore.BotStreamWriter.Flush();
          //
          //Join Rooms
          //
          if(BotCore.BotJoinedRooms == false){
          BotCore.BotStreamWriter.WriteLine($"JOIN #{BotCore.BotSettings.BotChannel}");
          BotCore.BotStreamWriter.Flush();
          BotCore.BotStreamWriter.WriteLine($"{BotCore.BotMessagePrefix}#{BotCore.BotSettings.BotChannel} :/color OrangeRed");
          BotCore.BotStreamWriter.Flush();
          BotCore.BotJoinedRooms = true;
          }
        }
    }
    public async Task SendMessage(string message){
        await BotCore.BotStreamWriter.WriteLineAsync($"{BotCore.BotMessagePrefix}#{BotCore.BotSettings.BotChannel} :/me [O_O] {message}");
        BotCore.PrintUsingColor(ConsoleColor.Yellow,"[O_O]",message);
        BotCore.BotStreamWriter.Flush();

    }
    public async Task SendWhisperMessage(string message, string userName){
        await BotCore.BotStreamWriter.WriteLineAsync($"{BotCore.BotMessagePrefix}#{BotCore.BotSettings.BotChannel} :/w {userName} {message}");
        BotCore.PrintUsingColor(ConsoleColor.Magenta,"[O_O]",message);
        BotCore.BotStreamWriter.Flush();
    }
    public async Task<string> RandomGreetings(){
      string tempString = "Hi";
      if(File.Exists("Data\\Greetings.json")){
        tempString = File.ReadAllText("Data\\Greetings.json");
        await Task.Delay(10);
        var listOfGreets = JsonConvert.DeserializeObject<List<string>>(tempString);
        Random rand = new Random();
        tempString = listOfGreets[rand.Next(listOfGreets.Count)];
      }
      return tempString;
    }
    public async Task CheckForUser(string userName){
      User usr;
      if(!BotCore.DictionaryOfUsers.TryGetValue(userName.ToLower(), out usr)){
        usr = new User() {
          UserName = userName          
        };
        BotCore.DictionaryOfUsers.Add(userName.ToLower(),usr);
      }else{
        usr.NumberOfMessages++;
        await Task.Delay(1);
      }
    }
    public async Task SaveData(){
        Console.Clear();
        BotCore.PrintUsingColor(ConsoleColor.Red,"[O_O]","SAVING");
        //
        //Save Chat log
        //
        var tempList = new List<string>();
        tempList.AddRange(BotCore.BotLogMessageList);
        BotCore.BotLogMessageList.Clear();
        File.AppendAllLines($"Logs\\{DateTime.Now.Day.ToString()} {DateTime.Now.Month.ToString()} {DateTime.Now.Year.ToString()} Chat Log.txt", tempList);
        await Task.Delay(100);
        //
        //Save Users
        //
        string jason = JsonConvert.SerializeObject(BotCore.DictionaryOfUsers);
        File.WriteAllText("Data\\Users.json", jason);
        BotCore.PrintUsingColor(ConsoleColor.Green,"[O_O]","SAVING COMPLETE");
    }    
    public async Task<string> RandomFiller(){
      string tempString = "Hi";
      if(File.Exists("Data\\Filler.json")){
        tempString = File.ReadAllText("Data\\Filler.json");
        await Task.Delay(10);
        var listOfFiller = JsonConvert.DeserializeObject<List<string>>(tempString);
        Random rand = new Random();
        tempString = listOfFiller[rand.Next(listOfFiller.Count)];
      }
      return tempString;
    }

    public async Task AddXpToUser(string userName, int amountOfXp){
        Random rand = new Random();
        int amntToadd = rand.Next(amountOfXp,amountOfXp*amountOfXp+1);
        User usr;
        if(BotCore.DictionaryOfUsers.TryGetValue(userName,out usr)){
          if(usr.UserLVL<50){
            usr.UserXp += amntToadd;
            if(usr.UserXp >= usr.UserXpToLvlUp){
              usr.UserLVL++;
              usr.UserXpToLvlUp = (usr.UserLVL*usr.UserLVL) * 50 * usr.UserLVL;
              if(usr.UserLVL == 50){
                  await SendMessage($"TwitchRPG {userName} dosáhl maximálního levelu 50");

              }else{
                  await SendMessage($"TwitchRPG {userName} dosáhl nového levelu {usr.UserLVL}, XP do dalšího levelu {usr.UserXpToLvlUp-usr.UserXp}");
              }
            }
          }
        }
    }
  }
}