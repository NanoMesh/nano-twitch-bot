using System;

namespace NanoTwitchBot
{
  public class Collectable{
      public string CollectableName { get; set; }
      public bool CollectableActive { get; set; }
      public DateTime CollectableLastTimeSent { get; set; }
      public int CollectableDelay { get; set; }

      public Collectable(){
          CollectableName = "Default Name";
          CollectableActive = false;
          CollectableLastTimeSent = DateTime.Now;
      }
  }
}