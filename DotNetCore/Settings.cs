namespace NanoTwitchBot
{
  public class Settings
  {

      public string BotUserName { get; set; }
      public string BotPassword { get; set; }
      public string BotChannel { get; set; }
      public Settings(){
          BotUserName = "Default User Name Please Change";
          BotPassword = "Default password, please obtain password from http://www.twitchapps.com/tmi/";
          BotChannel = "Default Channel to Join Please Change";
      }


  }

}