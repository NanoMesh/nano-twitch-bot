namespace NanoTwitchBot
{
  public class User{

    public string UserName { get; set; }
    public int NumberOfMessages { get; set; }
    public int NumberOfCookies { get; set; }
    public int NumberOfTacos { get; set; }
    public int NumberOfOrcs { get; set; }
    public int UserXp { get; set; }
    public int UserXpToLvlUp { get; set; }
    public int UserLVL { get; set; }
    public User(){
        UserName = "Default User Name";
        NumberOfMessages = 0;
        NumberOfCookies = 0;
        NumberOfTacos = 0;
        NumberOfOrcs = 0;
        UserXp = 0;
        UserXpToLvlUp = 50; 
        UserLVL = 1;
    }
  }
}
